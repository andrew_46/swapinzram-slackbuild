# swapinzram for Slackware
Heavily modified swapinzram script by Didier Spaier  
http://slackware.uk/slint/x86_64/slint-15.0/source/swapinzram/


The package swapinzram is intended to extend the swap spaces available beyond
swap partitions and swap files, providing swap space in the form of a block
device in compressed RAM, using the zram kernel module. As the files in it are
compressed this results in an increase of the RAM size usable by the system at
the cost of a small overhead to compress and decompress the files. 

This can be useful to:
1. Avoid or at least delay swapping on a mass storage device when available
space in RAM decreases. This results in a performance gain because writing in
RAM is way faster that on a hard disk or even an SSD.
2. Less writing on a storage devices like flash drives, eMMC, USB flash drives
or SD card, if the swap partition or file is installed on such devices.
Beyond the performance gain this also minimizes wearing of the device.

zram can also be used to create block devices in RAM for other usages, like
to store the files in /tmp or the kernel log, but this is not in the scope
of this package. However as we pick an available zram device id there should
be no conflict when adding these features. 

Also in the TODO list, allow to write idle/incompressible pages to a backing
storage rather than keeping them in memory. The documentation states that the
backing storage should be a swap partition, not a swap file, but I will check
if it's still true.

The script /etc/rc.d/rc.swapinzram allows to create or remove a swapinzram device
and check its status.  
**By default, it creates a device (zramX) for each core, with a 2G swap size**  

To start rc.swapinzram on boot, add this in /etc/rc.d/rc.local :  

```
if [ -x /etc/rc.d/rc.swapinzram ]; then
  /etc/rc.d/rc.swapinzram start
fi
```  

The default settings can be changed: edit as root the file /etc/swapinzram.conf,
which also describes them. If swapinzram is active, after having modified the
settings you can just type: "swapinzram restart" to put them into effect.

**New addition**  
File /etc/sysctl.d/zram.conf  
```
vm.vfs_cache_pressure=500
vm.dirty_background_ratio=1
vm.dirty_ratio=50
```

#### Important : remember to add "swapinzram-*" in /etc/slackpkg/blacklist

HOWTO:  
``git clone https://gitlab.com/maravtdm/swapinzram-slackbuild``  
and  
https://slackbuilds.org/howto/
